let express = require('express'), bodyParser = require('body-parser');
let mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

let app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

let port = process.env.port || 5000;


mongoose.connect('mongodb://admin:admin@ds157320.mlab.com:57320/calendar_db');


app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

var reminderSchema = new mongoose.Schema({
	title: 'string',
	description: 'string',
	start: 'string',
	end: 'string',
	color: 'string'
});

var ReminderModel = mongoose.model('reminder', reminderSchema);

app.get('/api/reminder', function(req,res) {
	ReminderModel.find({},function(err,docs) {
		if(err) {
			res.send(err);
		}
		else {
			res.send(docs);
		}
	});
});

app.post('/api/reminder', function(req,res) {
	let newReminder = new ReminderModel(req.body);
	newReminder.save(function (err) {
		if (err) {
			res.send('Data is not saved');
			return err;
		}
	});
  res.status(201).send(newReminder);
});

app.get('/api/reminder/:id', function(req,res) {
	ReminderModel.findById(req.params.id, function(err,docs) {
		if(err) {
			res.send(err);
		}
		else {
			res.send(docs);
		}
	});
});

app.put('/api/reminder/:id', function(req,res) {
	ReminderModel.findById(req.params.id, function(err, reminder) {
		if(err) {
			res.send(err);
		}
		else {
			reminder.title = req.body.title;
			reminder.description = req.body.description;
			reminder.start = req.body.start;
			reminder.end = req.body.end;
			reminder.color = req.body.color;
			reminder.save(function (err, updatedReminder) {
			    if (err) res.send(err);
			    res.send(updatedReminder);
			  });
		}
	});
});

app.delete('/api/reminder/:id', function(req,res) {
	ReminderModel.findByIdAndRemove(req.params.id, function(err) {
	    if (!err) {
        res.status(204).send('');
	    }
	    else {
		    res.send(err);
	    }
	});
});

//For avoidong Heroku $PORT error
app.get('/', function(request, response) {
    var result = 'App is running'+process.env.PORT;
    response.send(result);
}).listen(process.env.PORT || 5000);
